#include <Servo.h>

const int checkInterval = 100;

// ********** logging

const bool useSerial = true;

// ********** sensors

const int sensor1Pin = A0;
const int sensor2Pin = A1;
const int sensor3Pin = A2;
const int sensorThreshold = 5;
int ref1;
int ref2;
int ref3;
int value1;
int value2;
int value3;

// ********** lock

const int servoPin = 9;
const int lockDegree = 90;
const int unlockDegree = 180;
Servo servo;

// ********** button

const int buttonPin = 2;
const unsigned long pressTimeLock = 5000;
const unsigned long pressTimeUnlock = 60000;
bool pressed;
unsigned long pressStartMs = 0;
unsigned long pressTimeMs;

// ********** powerbank impuls

const int impulsPin = 10;
const unsigned long impulsIntervalMs = 30000;
const unsigned long impulsDurationMs = 1000;
unsigned long impulsStartMs = 0;
unsigned long impulsTimeMs;
bool impulsActive = false;

void setup()
{
  initLogging();
  
  initSensors();
  
  initLock();
  
  initButton();

  initImpuls();
}

void loop()
{
  checkSensors();

  checkButton();

  checkImpuls();
  
  delay(checkInterval);
}

// ********** logging

void initLogging()
{
  if (useSerial)
  {
    Serial.begin(9600);
  }
}

// ********** sensors

void initSensors()
{
  ref1 = getRef(sensor1Pin);
  ref2 = getRef(sensor2Pin);
  ref3 = getRef(sensor3Pin);
}

void checkSensors()
{
  value1 = getValue(sensor1Pin, ref1);
  value2 = getValue(sensor2Pin, ref2);
  value3 = getValue(sensor3Pin, ref3);

  logSensors();

  if (value1 > sensorThreshold &&
      value2 > sensorThreshold &&
      value3 > sensorThreshold)
  {
    unlock();
  }
}

void logSensors()
{
  if (useSerial)
  {
    Serial.print("S1=");
    Serial.print(value1);
    Serial.print("\tS2=");
    Serial.print(value2);
    Serial.print("\tS3=");
    Serial.println(value3);
  }
}

int getRef(int sensorPin)
{
  int refValue = analogRead(sensorPin);
  for (int i = 0; i < 10; i++)
  {
    refValue += analogRead(sensorPin);
    refValue /= 2;
  }
  return refValue;
}

int getValue(int sensorPin, int ref)
{
  int value = analogRead(sensorPin) - ref;
  value = abs(value);
  return value;
}

// ********** lock

void initLock()
{
  servo.attach(servoPin);
}

void lock()
{
  servo.write(lockDegree);
}

void unlock()
{
  servo.write(unlockDegree);
}

// button

void initButton()
{
  pinMode(buttonPin, INPUT_PULLUP);
}

void checkButton()
{
  pressed = !bool(digitalRead(buttonPin));

  if (pressed)
  {
    if (pressStartMs == 0)
    {
      pressStartMs = millis();
    }
    else
    {
      pressTimeMs = millis() - pressStartMs;
      if (pressTimeMs > pressTimeUnlock)
        unlock();
      else if (pressTimeMs > pressTimeLock)
        lock();
    }
  }
  else
  {
    pressStartMs = 0;
    pressTimeMs = 0;
  }
  
  //logButton();
}

void logButton()
{
  if (useSerial)
  {
    Serial.print("\t\t\tB=");
    Serial.print(pressed);
    Serial.print("\tS=");
    Serial.print(pressStartMs);
    Serial.print("\tT=");
    Serial.println(pressTimeMs);
  }
}

// ********** powerbank impuls

void initImpuls()
{
  pinMode(impulsPin, OUTPUT);
}

void checkImpuls()
{
  impulsTimeMs = millis() - impulsStartMs;
  if (impulsActive)
  {
    if (impulsTimeMs > impulsDurationMs)
    {
      impulsOff();
    }
  }
  else
  {
    if (impulsTimeMs > impulsIntervalMs)
    {
      impulsStartMs = millis();
      impulsOn();
    }
  }
  //logImpuls();
}

void impulsOn()
{
  digitalWrite(impulsPin, HIGH);
  impulsActive = true;
}

void impulsOff()
{
  digitalWrite(impulsPin, LOW);
  impulsActive = false;
}

void logImpuls()
{
  if (useSerial)
  {
    Serial.print("\t\t\t\t\t\tA=");
    Serial.print(impulsActive);
    Serial.print("\tS=");
    Serial.print(impulsStartMs);
    Serial.print("\tT=");
    Serial.println(impulsTimeMs);
  }
}
